from locust import HttpUser, task, between
import json

class MyUser(HttpUser):
    wait_time = between(1, 3)
    host = "https://reqres.in"

    @task
    def create_user(self):
        headers = {'Content-Type': 'application/json'}
        payload = {"name": "Thanos", "job": "Destroyer"}
        res = self.client.post("/api/users", data=json.dumps(payload), headers=headers)
        print(res.status_code)
        print(res.headers)
        print(res.elapsed)


# locust -f .\tasks\loadtest.py -u 1 -t 5 --headless