from flask import Flask, request, jsonify

app = Flask(__name__)


@app.route('/submit-json', methods=['POST'])
def submit_json():
    data = request.get_json()
    name = data['Pavan']
    email = data['pavan@gmail.com']
    # Process JSON data
    # Return a JSON response with a 201 status code
    response_data = {'message': 'JSON Data received successfully', 'name': name, 'email': email}
    return jsonify(response_data), 201


@app.route('/submit-form', methods=['POST'])
def submit_form():
    name = request.form.get('Pavan')
    email = request.form.get('pavan@gmail.com')

    # Process form data and create a new resource
    # Return a JSON response with a 201 status code for successful creation
    response_data = {'message': 'Form Data received and processed successfully', 'name': name, 'email': email}
    return jsonify(response_data), 201


if __name__ == '__main__':
    app.run(debug=True)
