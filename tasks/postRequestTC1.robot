*** Settings ***
Library         RequestsLibrary
Library         OperatingSystem
Library         Collections
Library         JSONLibrary

*** Variables ***
${base_url}     https://reqres.in
${endpoint}     /api/users
${filepath}     ${EXECDIR}//tasks/payload.json

*** Test Cases ***
Post Request with Valid Payload TO Create new User
    [Tags]      test1
    ${session headers}    Create Dictionary    Content-Type=application/json
    Create Session    ReqresSession    ${base_url}
    ${payload}    Get File    ${filepath}
    ${response}    POST On Session    ReqresSession    ${endpoint}    data=${payload}    headers=${session headers}
    Should Be Equal As Numbers    ${response.status_code}    201
    ${json_response}    Set Variable    ${response.json()}
    ${id}=      Collections.Get From Dictionary    ${json_response}    id
    Log    New User Created with ID: ${json_response["id"]}

POST With Empty JSON Payload
    [Tags]    empty-payload
    Create Session    session2    ${base_url}   disable_warnings=1
    ${response}=    POST On Session     session2    ${endpoint}
    Log To Console    ${response.status_code}
    Log To Console    ${response.content}
    Should Be Equal As Strings    ${response.status_code}    201
    ${json_response}=    Evaluate    json.loads('''${response.content}''')
    ${id}=      Collections.Get From Dictionary    ${json_response}    id

With Invalid Payload
#Body parameter is missing
    [Tags]    invalid-test
    ${invalid_json}=    Set Variable    1
    Create Session    session1    ${base_url}   disable_warnings=1
    ${response}=    POST On Session     session1    ${endpoint}     data=${invalid_json}
    Log To Console    ${response.status_code}
    Log To Console    ${response.content}
    Should Be Equal As Strings    ${response.headers['content-type']}    application/json; charset=utf-8
    Should Not Contain    ${response.text}    Invalid JSON

Send GET Request to Post Endpoint
    [Tags]      getreq
    Create Session    session1    https://reqres.in   disable_warnings=1
    ${response}     GET On Session      session1    ${endpoint}
    Should Not Be Equal As Strings    ${response.status_code}    201
