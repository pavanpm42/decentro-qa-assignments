
cd existing_repo
git remote add origin https://gitlab.com/pavanpm42/decentro-qa-assignments.git
git branch -M main
git push -uf origin main

Clone the project: 
   Using SSH: git@gitlab.com:pavanpm42/decentro-qa-assignments.git
   Using Https: https://gitlab.com/pavanpm42/decentro-qa-assignments.git

=============================================== Pre-requisites =======================================================
1. Python should be installed with a Version > 3.6 
2. Once after cloning, Open the project using IDE >> Set up the Virtual Environment in the project directory.
          Steps to set up Local Environment: Open terminal run below commands
            `python -m venv venv`
            `venv\Scripts\activate` (For Windows) `source venv/bin/activate` (Mac/iOS)
3. Ensure you've installed below plugin, if not please use the commands to install:
        robot framework: `pip install robotframework`
        robot framework-request: `pip install robotframework-requests`
        robot framework-jsonlibrary: `pip install robotframework-jsonlibrary`
        robot framework-request: `pip install requests`
        locust: `pip install locust` 
        flask: `pip install Flask` 

==================================== **TASK 1: Creating Flask Sever** ===================================
1. Run [FlaskSever1.py](tasks/FlaskSever1.py) file which is present under [tasks](tasks/) directory.
2. Flask server will start running on the port 5000, with an IP http://127.0.0.1:5000
3. We can utilize Postman/Chrome dev tool to verify API with a proper endpoints.
        To handle an API with JSON Data : _'/submit-json'_
        To handle an API with Form Data : _'/submit-form'_


==================================== **TASK 2: Automation Testing** =====================================
1. Open terminal within the IDE run this command: `robot .\\tasks\postRequestTC1.robot`
2. We can see the results on the terminal
3. If we want to change the input values we can modify [payload.json](tasks/payload.json)


==================================== **TASK 3: Load Testing** ===========================================
1. Run the command to perform Load testing using [loadtest.py](tasks/loadtest.py)
            `locust -f .\tasks\loadtest.py`
2. Locust server will be running at  http://localhost:8089, If we tap on this URL, Locust home page will be displayed.
3. Here we can input the required data and start performing load testing on that particular website results will be displayed the same browser window
4. If we want to perform load testing in headless mode, use below command:
        `locust -f .\tasks\loadtest.py -u 1 -t 10 --headless` 
                            where u refers to number users & t refers to time limit
5. Please update the host with a new url to perform load testing on new website in [loadtest.py](tasks/loadtest.py) 